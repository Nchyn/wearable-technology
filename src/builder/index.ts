import { copy, ensureDir, readFile, writeFile } from 'fs-extra';
import { join, resolve } from 'path';
import { Data, Folder } from '../Data';
import { TagSpec, TagsSpec } from '../TagsSpec';
import { chaptersDir, distChaptersDir, distDir, rootDir, staticDir } from './dirs';
import { ErrorReporter } from './ErrorReporter';
import { log } from './indentConsole';
import { LoaderContext } from './LoaderContext';
import { load } from './loaders/Loader';
import { Stats } from './Stats';
import { loadTagsSpec, validateAndBuildTagMap } from './tagsSpecParser';
import yargs = require('yargs');

const argv = yargs.options({
  production: { type: 'boolean', default: false },
  suppressError: { type: 'boolean', default: false, alias: 'suppress-error' },
}).argv;

(async () => {
  const errorReporter = new ErrorReporter();
  const startTime = Date.now();

  await ensureDir(distChaptersDir);

  // Copy static
  await copy(staticDir, distDir);
  const indexPath = resolve(distDir, 'index.html');
  const nowTime = new Date().getTime();
  let result = await readFile(indexPath, 'utf8');
  result = result.replace(new RegExp('js" defer>', 'g'), 'js?v=' + nowTime + '" defer>');
  result = result.replace(new RegExp('css">', 'g'), 'css?v=' + nowTime + '">');
  await writeFile(indexPath, result, 'utf8');
  log('[[green|Static copied.]]');

  // Load tags spec
  let tagsSpec: null | TagsSpec = null;
  let tagsMap: null | ReadonlyMap<string, TagSpec> = null;
  let tagAliasMap: null | ReadonlyMap<string, string> = null;
  try {
    tagsSpec = await loadTagsSpec();
    ({ tagAliasMap, tagsMap } = await validateAndBuildTagMap(tagsSpec));
  } catch (error) {
    errorReporter.wrapAndReportError('Failed to load tags spec.', error);
  }

  if (tagsSpec !== null) {
    await writeFile(
      resolve(distDir, 'tagsSpec.json'),
      JSON.stringify(tagsSpec, null, argv.production ? 0 : 2),
    );
  }

  const stats = new Stats(argv.production);

  const rootLoaderCtx = new LoaderContext(
    true,
    chaptersDir,
    '',
    stats,
    argv.production,
    errorReporter,
    tagsMap,
  );

  const data: Data = {
    chapterTree: await load(rootLoaderCtx)! as Folder,
    charsCount: argv.production ? stats.getCharsCount() : null,
    paragraphsCount: stats.getParagraphCount(),
    keywordsCount: [...stats.getKeywordsCount()].sort((a, b) => b[1] - a[1]),
    buildNumber: process.env.CI_PIPELINE_IID || 'Unoffical',
    authorsInfo: JSON.parse(await readFile(join(rootDir, 'authors.json'), 'utf8')),
    buildError: errorReporter.hasError(),
    tags: tagsSpec === null ? [] : tagsSpec
      .sort((a, b) => b.priority - a.priority)
      .map(tagSpec => [
        tagSpec.tag,
        tagSpec.variants,
      ]),
    tagAliases: Array.from(tagAliasMap ?? []),
  };
  await writeFile(
    resolve(distDir, 'data.js'),
    `window.DATA=${JSON.stringify(data, null, argv.production ? 0 : 2)};`,
  );
  log('[[green|data.js created.]]');
  log(`[[green|Time spent: [[cyan|${Date.now() - startTime}ms]].]]`);

  if (errorReporter.hasError()) {
    log();
    errorReporter.printAll();
    if (!argv.suppressError) {
      process.exit(1);
    }
  }
})();
