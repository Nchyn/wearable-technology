import { $e } from '../$e';
import { tagCountMap } from '../data/data';

export function tagSpan(tag: string, active: boolean) {
  const count = tagCountMap.get(tag) ?? 0;
  return (
    <span
      className={ 'tag' + (active ? ' active' : '') + ((count === 0) ? ' empty' : '')}
    >
      <span className={ 'text' + (tag.endsWith('）') ? ' parentheses-ending' : '') }>{ tag }</span>
      <span className='count'>{ count }</span>
    </span>
  );
}